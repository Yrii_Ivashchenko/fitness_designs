let panini = require('panini'),
    ext_replace = require('gulp-ext-replace');

module.exports = function () {
    $.gulp.task('panini', () => {
        return $.gulp.src('./dev/panini/pages/**/*.{html,hbs,handlebars}')
            .pipe(panini({
                root: './dev/panini/pages/',
                layouts: './dev/panini/layouts/',
                // pageLayouts: {
                //     // All pages inside src/pages/blog will use the blog.html layout
                //     'blog': 'blog'
                // }
                partials: './dev/panini/partials/',
                helpers: './dev/panini/helpers/',
                data: './dev/panini/data/'
            }))
            .pipe(ext_replace('.html'))
            .pipe($.gulp.dest('./build/'))

            .on('end', $.browserSync.reload);

    });
};
