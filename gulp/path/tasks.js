module.exports = [
    './gulp/tasks/clean',
    './gulp/tasks/fonts',
    './gulp/tasks/copyLib',
    './gulp/tasks/image',
    './gulp/tasks/panini',
    './gulp/tasks/resetPages',
    './gulp/tasks/scripts',
    './gulp/tasks/serve',
    './gulp/tasks/styles',
    './gulp/tasks/svg',
    './gulp/tasks/watch'
];