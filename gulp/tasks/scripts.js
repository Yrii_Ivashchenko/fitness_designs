const uglify = require("gulp-uglify"),
  concat = require("gulp-concat"),
  sourcemaps = require("gulp-sourcemaps"),
  filesize = require("gulp-filesize"),
  scriptsPATH = {
    input: "./dev/static/js/",
    output: "./build/static/js/",
  },
  babel = require("gulp-babel");

module.exports = function () {
  $.gulp.task("libsJS:dev", () => {
    return $.gulp
      .src([
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/svg4everybody/dist/svg4everybody.min.js",
        "node_modules/wowjs/dist/wow.min.js",
      ])
      .pipe(concat("libs.min.js"))
      .pipe($.gulp.dest(scriptsPATH.output));
  });

  $.gulp.task("libsJS:build", () => {
    return $.gulp
      .src([
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/svg4everybody/dist/svg4everybody.min.js",
        "node_modules/wowjs/dist/wow.min.js",
      ])
      .pipe(concat("libs.min.js"))
      .pipe(uglify())
      .pipe($.gulp.dest(scriptsPATH.output));
  });

  $.gulp.task("js:dev", () => {
    return $.gulp
      .src([
        scriptsPATH.input + "*.js",
        "!" + scriptsPATH.input + "libs.min.js",
      ])
      .pipe(sourcemaps.init())
      .pipe(
        babel({
          presets: ["@babel/env"],
        })
      )
      .pipe(sourcemaps.write("."))
      .pipe($.gulp.dest(scriptsPATH.output))
      .pipe(filesize())
      .pipe(
        $.browserSync.reload({
          stream: true,
        })
      );
  });

  $.gulp.task("js:build", () => {
    return $.gulp
      .src([
        scriptsPATH.input + "*.js",
        "!" + scriptsPATH.input + "libs.min.js",
      ])
      .pipe(
        babel({
          presets: ["@babel/env"],
        })
      )
      .pipe(concat("main.min.js"))
      .pipe(uglify())
      .pipe($.gulp.dest(scriptsPATH.output))
      .pipe(filesize());
  });

  $.gulp.task("js:build-min", () => {
    return $.gulp
      .src([
        scriptsPATH.input + "*.js",
        "!" + scriptsPATH.input + "libs.min.js",
      ])
      .pipe(concat("main.min.js"))
      .pipe(uglify())
      .pipe($.gulp.dest(scriptsPATH.output));
  });
};
