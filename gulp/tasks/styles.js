const plumber = require('gulp-plumber'),
      scss = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      concat  = require('gulp-concat'),
      cleancss = require('gulp-clean-css'),
      filesize = require('gulp-filesize'),
      postcss = require('gulp-postcss'),
      sourcemaps = require('gulp-sourcemaps'),
      stylesPATH = {
          "input": "./dev/static/styles/",
          "output": "./build/static/css/"
      };

module.exports = function () {
    $.gulp.task('styles:dev', () => {
        var plugins = [
            require('postcss-sort-media-queries'),
            require('postcss-momentum-scrolling')
        ];
        return $.gulp.src(stylesPATH.input + 'styles.scss')
            .pipe(plumber())
            .pipe(sourcemaps.init())
            .pipe(scss({ outputStyle: 'expanded' }))
            .pipe(concat('styles.min.css'))
            .pipe(postcss(plugins))
            .pipe(autoprefixer({
                 overrideBrowserslist:  ["defaults"]
            }))
            .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
            .pipe(sourcemaps.write())
            .pipe($.gulp.dest(stylesPATH.output))
            .pipe(filesize())
            .on('end', $.browserSync.reload);
    });
    $.gulp.task('styles:build', () => {
        var plugins = [
            require('postcss-sort-media-queries'),
            require('postcss-momentum-scrolling')
        ];
        return $.gulp.src(stylesPATH.input + 'styles.scss')
            .pipe(scss())
            .pipe(concat('styles.min.css'))
            .pipe(postcss(plugins))
            .pipe(autoprefixer({
                overrideBrowserslist: ['last 3 versions']
            }))
            .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
            .pipe($.gulp.dest(stylesPATH.output))
            .pipe(filesize())
    });
    $.gulp.task('styles:build-min', () => {
        return $.gulp.src(stylesPATH.input + 'styles.scss')
            .pipe(scss())
            .pipe(concat('styles.min.css'))
            .pipe(autoprefixer({
                overrideBrowserslist: ['last 3 versions']
            }))
            .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
            .pipe($.gulp.dest(stylesPATH.output))
            .pipe(filesize())
    });
};
