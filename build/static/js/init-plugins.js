"use strict";

document.addEventListener("DOMContentLoaded", e => {
  /*
    wow anim scroll
   */
  // возвращает куки с указанным name,
  // или undefined, если ничего не найдено
  function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
  /* preloader */


  if ($('.preloader').length && !getCookie("preloader-fit")) {
    var tweenTime = 4; //sec

    var master = new TimelineMax({
      delay: tweenTime - 1.2
    });
    var tweenTime = 4; //sec

    master.eventCallback('onComplete', function () {
      new WOW().init();
    });
    preloader();

    function preloader() {
      var tl = new TimelineMax({
        paused: true
      });
      tl.set('body', {
        className: '+=no-scroll'
      }).set('.preloader', {
        opacity: 1
      }).addLabel('first').to('.preloader__logo', 1.5, {
        opacity: 1,
        scale: 1,
        webkitFilter: 'blur(0px)',
        ease: 'Power3.easeInOut'
      }).to('.preloader__progress span', 1, {
        width: '100%',
        ease: 'Power3.easeInOut'
      }, 'first+=.5').to('.preloader', 1.3, {
        delay: 1,
        opacity: 0,
        zIndex: -1,
        ease: 'Power3.easeInOut'
      }).to('.preloader__wrap', .5, {
        yPercent: '100%',
        opacity: 0,
        ease: 'Power1.easeIn'
      }, '-=1.5').to('body', 0, {
        className: '-=no-scroll'
      }, '-=1.0');
      tl.duration(tweenTime).play();
      console.log(tl.endTime());
      return tl;
    }

    ;
    document.cookie = "preloader-fit=yes";
  } else if (document.querySelector('.preloader')) {
    document.querySelector('.preloader').remove();
    new WOW().init();
  } else if (!document.querySelector('.preloader')) {
    new WOW().init();
  }
  /* preloader end */

});
//# sourceMappingURL=init-plugins.js.map
