"use strict";

document.addEventListener("DOMContentLoaded", e => {
  //  Nav
  let burger = document.querySelector(".js__burger"),
      header = document.querySelector("header"),
      body = document.querySelector("body");
  burger.addEventListener("click", () => {
    burger.classList.toggle("active");
    body.classList.toggle("overflow");
    header.classList.toggle("active");
  });
  /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */

  let prevScrollpos = window.pageYOffset;

  window.onscroll = function () {
    let currentScrollPos = window.pageYOffset;
    prevScrollpos = currentScrollPos;

    if (prevScrollpos > 50) {
      header.classList.add("bg");
    } else {
      header.classList.remove("bg");
    }

    $(".js__overlay").css({
      opacity: function () {
        let elementHeight = $(this).height(),
            opacity = (1 - (elementHeight - prevScrollpos) / elementHeight) * 0.8 + 0.55;
        return opacity;
      }
    });
  }; // Maximum height in a set of elements
  // $(elements).height( getMaxHeight($(elements)) );


  let getMaxHeight = function ($elms) {
    let maxHeight = 0;
    $elms.each(function () {
      // In some cases you may want to use outerHeight() instead
      let height = $(this).height();

      if (height > maxHeight) {
        maxHeight = height;
      }
    });
    return maxHeight;
  };

  $('.time').css('height', 500 * $('.time img').length);
  $(window).on("resize", function () {
    $(".js__height").css("height", "");
    $(".js__height").height(getMaxHeight($(".js__height")));
    $(".js__two-height").css("height", "");
    $(".js__two-height").height(getMaxHeight($(".js__two-height")));
  });
  setTimeout(() => {
    $(".js__height").height(getMaxHeight($(".js__height")));
    $(".js__two-height").height(getMaxHeight($(".js__two-height")));
  }, 200); // get year

  document.getElementById("year-js").textContent = new Date().getFullYear();
  let initScrollTop = $(window).scrollTop(); // Set the image's vertical background position based on the scroll top when the page is loaded.

  $('.banner-scroll').css({
    'background-position-y': initScrollTop / 75 + '%'
  }); // When the user scrolls...

  $(window).scroll(function () {
    // Find the new scroll top.
    let scrollTop = $(window).scrollTop(); // Set the new background position.

    $('.banner-scroll').css({
      'background-position-y': scrollTop / 75 + '%'
    });
  });
});
//# sourceMappingURL=main.js.map
